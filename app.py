import mmcv
import datetime
from dash import Dash,html,dcc,callback,Output,Input,State,ctx, MATCH, ALL, Patch,no_update
from dash_canvas import DashCanvas
from  utils.components import* #canvas,nav_option,brush,pallete,navbar,csv_table,image_canvas,canvas_figure
from utils.file_io import upload_file,get_image_from_file
from utils.utils import random_rgb,rgba_to_rgb,bounding_box_annotation,parse_canvas_json_data
import dash_bootstrap_components as dbc
import dash_mantine_components as dmc
import os
import csv
import json
from utils.Image_editing import*
from utils.parsers import*
from dash.exceptions import PreventUpdate
from plotly import io as pio
import plotly.graph_objs as go
import numpy as np
import dash_bootstrap_components as dbc

color_map = {"dark_clay":'#80A3A2',
             'blue_clay':'#ABCECF',
             'light_clay':'#C4DCE0',
             'light_blue':'#DAF4F5'}


CLASS_MAP = {} 
CURRENT_COLOR = random_rgb()
DEFAULT_BRUSH_SIZE = 5
DEFAULT_IMAGE_PATH = 'assets/images/test.jpeg'
class_color_map = {}

style_sheets = [dbc.themes.BOOTSTRAP,'/assets/style.css']
app = Dash(__name__, external_stylesheets=style_sheets,   meta_tags=[
        {"name": "viewport", "content": "width=device-width, initial-scale=1"}
    ])
app.css.config.serve_locally = True

columns =  ['class','x1', 'y1', 'x2', 'y2', 'width', 'height']

class_list =  html.Div(
                            [
                                html.Div("Class List"),
                                dcc.Input(id="new-item-input",style={'width':'7vw','background-color':color_map['dark_clay'],'border-radius':'5px'}),
                                html.Button(id='class-color-button',style={'margin-left':'0.2vw','background-color':CURRENT_COLOR,'position':'absolute','width':'1.5vw','height':'2.2vh','border-radius':'5px'}),
                                html.Button("Add", id="add-btn",style={'background-color':color_map['blue_clay'],'width':'3vw','font-size':'0.5vw','border-radius':'5px'}),
                                html.Button("Clear", id="clear-done-btn",style={'background-color':color_map['blue_clay'],'width':'4vw','font-size':'0.5vw','border-radius':'5px'}),
                                html.Div(id="list-container-div"),
                                html.Div(id="totals-div"),
                            ],style={'margin-top':'10vh','margin-left':'88.3vw','position':'absolute','width':'7vw'}
                        )



BASE_IMAGE = get_image_from_file('test.jpeg')
test_canvas = image_canvas(BASE_IMAGE,brush_size=DEFAULT_BRUSH_SIZE,brush_color=CURRENT_COLOR)

table = csv_table('Csv-Table',columns,'csv-table')
brush_slider = brush('Brush-Div','Brush-Div',[0,100],10,'Brush Size')
color_pallete = pallete('Color-Pallete','color-picker','rgba',"rgba(41, 96, 214, 1)",'Color Pallete',brush_slider,'color-div')

app_navbar = navbar()
dummy_canvas =  html.Div(canvas(None,id='canvas',contents=[]),style={'display':'none'},id='canvas-container')
file_options,nav_inputs = nav_option(['Upload','Export'],label='File',left_offset='0%',id='file')
tools_options,tools_inputs = nav_option(['Brush','Segmentation','Object Detection','Color Pallete'],label='Tools',left_offset='5%',id='Tools')
color_space_options,color_space_inputs = nav_option(['RGB','LAB','YCRCB','HSV','YUV'],label='Color Space Tranforms',left_offset='11%',id='Color_Spaces')

color_space_inputs.append(State('Canvas','figure'))
color_space_inputs.append(State('Default_image_path','data'))
color_space_inputs.append(State('filename','data'))

segmentation_tool_div = create_segmentation_tools(['R','G','B'])

tools_div = html.Div([color_pallete,segmentation_tool_div],id='Tools-Div',className='Tools',)

image_div = html.Div(
                        [
                            dcc.Upload(id='upload',
                                       children=html.Div(['Drag and Drop or',
                                                          html.A(' Select Files')
                                                          ],style={'margin-left':'30vw','margin-top':'20vh'}),
                                        style={'position':'absolute','width':'100%','height':'100%','background-color':'transparent','border-radius':'20px'}
                                       ),
                            html.Div(id="Image-Display"),
                            dummy_canvas,
                            test_canvas,
                        ],style={'display':'flex','position':'absolute','margin-top':'12vh','margin-left':'25vw','width':'51vw','height':'65vh','background-color':color_map['light_clay'],'border-radius':'20px'}
                        ,className='canvas-container',
                    )

main_div = html.Div([image_div,tools_div],id='center',className='Center-Div')

app.layout = html.Div(
                        [
                            dcc.Store(id='current-class-color'),
                            dcc.Store(id='class-dict',),
                            dcc.Store(id='Saved-Color',data=CURRENT_COLOR),
                            dcc.Store(id='annotation-table',),
                            dcc.Store(id='filename',),
                            dcc.Store(id='Canvas-Image',),
                            dcc.Store(id='Default_image_path',data='test.jpeg'),
                            dcc.Store(id='Color_space',),
                            app_navbar,
                            file_options,
                            tools_options,
                            color_space_options,
                            main_div,
                            table,
                            class_list
                        ],style={'overflow':'hidden','display':'flex','width':'100vw','height':'100vh','background-color':color_map['blue_clay']})



def parse_contents(contents, filename, date):
    filename = upload_file(filename)

    return html.Div([
                      canvas(filename,id='canvas',contents=contents)
                    ],style={'position':'absolute','margin-top':'0vh'})



@app.callback(
        Output('Canvas','figure',allow_duplicate=True),
        State('Canvas','figure'),
        Input('Top-Channel-Slider','value'),
        Input('Mid-Channel-Slider','value'),
        Input('Bottom-Channel-Slider','value'),
        State('Color_space','data'),
        State('filename','data'),
        State('Default_image_path','data'),
        prevent_initial_call=True)
def segment_figure(figure,top_channel_range,mid_channel_range,bottom_channel_range,color_space,filename,default_file):
    if filename is None:
        filename = default_file
        
    new_figure = channel_segmentation(filename,top_channel_range,mid_channel_range,bottom_channel_range,color_space)

    return new_figure




@app.callback(
        Output('Export-menu-button','style'),
        Input('Export-menu-button','n_clicks'),
        State('annotation-table','data'),
        State('filename','data'),
        prevent_initial_call=True
)
def export_annotation(click,annotation_dictinary,filename):
    file_base= filename.split('.')[0]
    csv_column_name = ['class','x1','y1','x2','y2','width','height']
    with open(file_base+'.csv','w') as csvfile:
        writer = csv.DictWriter(csvfile,fieldnames=csv_column_name)
        writer.writeheader()
        writer.writerows(annotation_dictinary)

    return no_update


@app.callback(
            Output('canvas','lineWidth'),
            Input('Brush-Slider','value'),
            prevent_initial_call=True
            )
def set_brush_size(value):
    return value


@app.callback(  
   
                Output('canvas','lineColor',allow_duplicate=True),
                Output('Canvas','figure',allow_duplicate=True),
                Input('color-picker','value'),
                Input("add-btn", "n_clicks"),
                Input('current-class-color','data'),
                State('Canvas','figure'),
                prevent_initial_call=True
            )
def set_brush_color(color,click,data,figure):   
    fig = go.Figure(figure)

    if click is not None:
        return data,fig
    else:
        data = rgba_to_rgb(color)
        print('color:',data)
        fig.update_layout({
            'newshape.line.color':data
        })

    return color,fig



@app.callback(
        Output('Segmentation-Div','style'),
        Input('Segmentation-menu-button','n_clicks'),
        prevent_initial_call=True
        )
def display_segmentation_tools(click):
      return{'position': 'absolute','width': '12vw','height': '20vh','background-color':'azure','border-radius':'20px','margin-top':'35vh','align-items': 'center'}



@app.callback(Output('color-div','style'),
             Input('Color Pallete-menu-button','n_clicks'),
             prevent_initial_call=True)
def create_pallete(value):
    return {'background-color':color_map['blue_clay'],'width':'10vw','height':'20vh','display':'block','position':'absolute','margin-top':'20%','margin-left':'2%','position':'absolute','border-radius':'20px'}



# Callback to add new item to list
@app.callback(
    Output("list-container-div", "children", allow_duplicate=True),
    Output("new-item-input", "value"),
    Output('canvas','lineColor',allow_duplicate=True),
    Output('current-class-color','data',allow_duplicate=True),
    Output('class-color-button','style'),
    Output('class-dict','data'),
    Input("add-btn", "n_clicks"),
    Input('current-class-color','data'),
    Input('class-dict','data'),
    Input('Saved-Color','data'),
    State("new-item-input", "value"),
    prevent_initial_call=True,
)
def add_item(button_clicked,current_color,class_dictionary,stored_data,value):
    patched_list = Patch()
    if ctx.triggered_id == 'current-class-color':
        return no_update,no_update,no_update,no_update,no_update,no_update
    new_color = random_rgb()

    if current_color is None:
        current_color = stored_data


    if class_dictionary is None:
            class_dictionary = {}
        
    class_dictionary[current_color] = value

    def new_checklist_item(): 
        return html.Div(
            [
                   dbc.Card( html.Button(value=value,id={"type":'button','index':button_clicked}, n_clicks=0,style={'margin-bottom':'2vh','margin-left':'0vw','background-color':current_color,'position':'absolute','width':'1.5vw','height':'2.2vh','border-radius':'5px'}),),
                html.Div(
                    [value],
                    style={"display": "inline", "margin-left": "2vw"},
                ),
             
            ]
        )

    patched_list.append(new_checklist_item())
    return patched_list, "",current_color,new_color,{'margin-left':'0.2vw','background-color':new_color,'position':'absolute','width':'1.5vw','height':'2.2vh','border-radius':'5px'},class_dictionary



# Callback to update item styling
@app.callback(
    Output({"index": MATCH, "type": "output-str"}, "style"),
    Input({"index": MATCH, "type": "done"}, "value"),
     Input('current-class-color','data'),
    prevent_initial_call=True,
)
def item_selected(input):
    if not input:
        style = {"display": "inline", "margin": "10px"}
    else:
        style = {
            "display": "inline",
            "margin": "10px",
            "textDecoration": "line-through",
            "color": "#888",
        }
    return style



# Callback to delete items marked as done
@app.callback(
    Output("list-container-div", "children", allow_duplicate=True),
    Input("clear-done-btn", "n_clicks"),
    State({"index": ALL, "type": "done"}, "value"),
    prevent_initial_call=True,
)
def delete_items(n_clicks, state):
    patched_list = Patch()
    values_to_remove = []
    for i, val in enumerate(state):
        if val:
            values_to_remove.insert(0, i)
    for v in values_to_remove:
        del patched_list[v]
    return patched_list




@app.callback(
    Output('canvas','lineColor',allow_duplicate=True),
    Output('current-class-color','data',allow_duplicate=True),
    Output('Canvas','figure',allow_duplicate=True),
     Input({"type": 'button','index':ALL}, 'n_clicks'),
     Input({"type": 'button','index':ALL}, 'style'),
    Input('current-class-color','data'),
    State('Canvas','figure'),
     prevent_initial_call= True
)
def update_current_class(value,style,color,figure):    
    fig = go.Figure(figure)

    if len(ctx.triggered) == 1:  
        button_color = style[ctx.triggered_id['index'] - 1]['background-color']
        fig.update_layout({
            'newshape.line.color':button_color
        })

        return button_color,button_color,fig

    fig.update_layout({
            'newshape.line.color':color
    })


    return color,color,fig


# 'RGB','LAB','YCRCB','HSV','YUV
@app.callback(
    Output('Image-Display','children',allow_duplicate=True),
    Output('Canvas','figure',allow_duplicate=True),
    Output('Color_space','data'),
    color_space_inputs,
    prevent_initial_call = True,
)
def change_color_space(*all_inputs):
    figure = all_inputs[5]
    file_name = all_inputs[-1]
    
    
    if file_name is None:
        file_name = all_inputs[6]
    print("file_name----------:",file_name)
    color_space = parse_color_space_choice(ctx.triggered_id)
    figure = convert_color_space(figure,new_color_space=color_space,image_file_name=file_name)

    return no_update,figure,color_space


@app.callback(
    Output('Canvas','figure',allow_duplicate=True),
    Output('filename','data'),
    Input('Upload-menu-button','filename'),
    prevent_initial_call=True
)
def upload_image(filename):
    image = get_image_from_file(filename)
    return canvas_figure(image),filename


@app.callback(
        Output('csv-table','data'),
        Input("Canvas", "relayoutData"),
        Input('class-dict','data'),
        prevent_initial_call = True
)
def update_annotation_table(annotation,class_list):
    if class_list is None:
        return no_update
    else:
       return parse_canvas_annotation(annotation,class_list)
    
@app.callback(Output('Canvas','figure',allow_duplicate=True),Input('binary-mask','value'),prevent_initial_call = True)
def set_binary_mask(value):
    print("set mask to binary")
    return no_update



if __name__ =='__main__':
    app.run_server(debug=True,port=8080,host='0.0.0.0')