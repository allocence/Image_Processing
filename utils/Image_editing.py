import cv2 as cv
import os
import PIL.Image
import numpy as np
from plotly import io as pio
from utils.components import canvas_figure
from utils.utils import*

BASE_PATH = 'assets/images'
TMP_PATH = 'assets/images/save_files/images/tmp'
COLOR_SPACES ={
    """--------------- Conversion based on RGB source color space ---------------"""
        'lab':cv.COLOR_BGR2LAB,
        'ycrcb':cv.COLOR_BGR2YCrCb,
        'hsv':cv.COLOR_BGR2HSV,
        'yuv':cv.COLOR_BGR2YUV,
        'rgb':cv.COLOR_BGR2RGB
}


def convert_color_space(figure,new_color_space,image_file_name):
    """
    DEF:
        Convertes the source figure into any other color space:
    """
    print("image path: ",image_file_name)
    # image = figure_to_image(figure)
    image_path = os.path.join(BASE_PATH,image_file_name)
    image = cv.imread(image_path)
    image = cv.cvtColor(image,COLOR_SPACES[new_color_space])

    figure = canvas_figure(image)

    return figure





def add_layout_images_to_fig(fig, images, update_ranges=True):
    """ images is a sequence of PIL Image objects """
    if len(images) <= 0:
        return fig
    for im in images:
        # if image is a path to an image, load the image to get its size
        width, height = _pilim_if_path(im).size
        # Add images
        fig.add_layout_image(
            dict(
                source=im,
                xref="x",
                yref="y",
                x=0,
                y=0,
                sizex=width,
                sizey=height,
                sizing="contain",
                layer="below",
            )
        )
    if update_ranges:
        width, height = [
            max([_pilim_if_path(im).size[i] for im in images]) for i in range(2)
        ]
        # TODO showgrid,showticklabels,zeroline should be passable to this
        # function
        fig.update_xaxes(
            showgrid=False, range=(0, width), showticklabels=False, zeroline=False
        )
        fig.update_yaxes(
            showgrid=False,
            scaleanchor="x",
            range=(height, 0),
            showticklabels=False,
            zeroline=False,
        )
    return fig


def _pilim_if_path(im):
    if type(im) == type(str()):
        return PIL.Image.open(im)
    return im


def default_figure(annotation=None,background_image = None,bush_size=None,brush_color=None):
    # print('background_image:',background_image)
    print('annotation: ',annotation) 
    if annotation is not None:
        print("^^^^^^^^^^^^^^^annotation[shapes]:",annotation['shapes'])


def figure_to_image(figure):
    # print("figure: ",figure)
    image_string = pio.to_image(figure,format='png')
    numpy_array = np.fromstring(image_string, np.uint8)
    image = cv.imdecode(numpy_array, cv.IMREAD_COLOR)

    return image

def channel_segmentation(image_file_name,top_range,mid_range,bottom_range,color_space):

    # print("image path: ",image_file_name)
    # # image = figure_to_image(figure)
    image_path = os.path.join(BASE_PATH,image_file_name)
    image = cv.imread(image_path)
    image = cv.cvtColor(image,COLOR_SPACES[color_space])

    lower_threshold = [top_range[0],mid_range[0],bottom_range[0]]
    upper_threshold = [top_range[1],mid_range[1],bottom_range[1]]
    thresholded_image = cv.inRange(image,np.array(lower_threshold),np.array(upper_threshold))
    # top,mid,bot = cv.split(image)
    # _,top_threshold = cv.threshold(top,top_range[0],top_range[1],cv.THRESH_BINARY)
    # _,mid_threshold = cv.threshold(mid,mid_range[0],mid_range[1],cv.THRESH_BINARY)
    # _,bot_threshold = cv.threshold(bot,bottom_range[0],bottom_range[1],cv.THRESH_BINARY)

    white_pixels = np.where(thresholded_image == 255)
    # print("white_pixls:",white_pixels)
    thresholded_image = cv.merge((thresholded_image,thresholded_image,thresholded_image))

    color = random_cv_color()
    print("color: ",color)
    thresholded_image[white_pixels] = color

    overlay = cv.addWeighted(thresholded_image,0.9,image,0.9,0)
    # white_pixels = np.where(thresholded_image == 255)

    figure = canvas_figure(overlay)

    return figure


def get_binary_mask(figure,top_range,mid_range,bottom_range):
    image = figure_to_image(figure)
    lower_threshold = [top_range[0],mid_range[0],bottom_range[0]]
    upper_threshold = [top_range[1],mid_range[1],bottom_range[1]]
    thresholded_image = cv.inRange(image,np.array(lower_threshold),np.array(upper_threshold))
    
    thresholded_figure = canvas_figure(cv.merge((thresholded_image,thresholded_image,thresholded_image)))

    return thresholded_figure


