from dash_canvas import DashCanvas
from dash_canvas.utils import (parse_jsonstring,image_string_to_PILImage,array_to_data_url
                               ,modify_segmentation)
import cv2 as cv
import dash_mantine_components as dmc
from skimage import io,color,segmentation,img_as_ubyte,filters,measure,data
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import plotly.express as px
from dash import html
from dash import dash_table
from dash import Input
import plotly.graph_objects as go

color_map = {"dark_clay":'#80A3A2',
             'blue_clay':'#ABCECF',
             'light_clay':'#C4DCE0',
             'light_blue':'#DAF4F5'}


def canvas(img_file=None,id='Canvas',line_width=2,contents=None):

    if img_file is None:
        height,width = 10,10
        canvas_width= 100
        canvas_height = round(height*canvas_width/width) 
        scale = canvas_width/width

        return  DashCanvas(id=id,
                        width=canvas_width,
                        height=canvas_height,
                        scale=scale,
                        lineWidth=line_width,
                        
                        )
    try:
        img = io.imread(img_file)
    except:
        print("file not found")
        img = data.coins()

    height,width = img.shape[:2]

    canvas_width= 1620
    canvas_height = round(height*canvas_width/width) 
    scale = canvas_width/width

    canvas = DashCanvas(id=id,
                        filename=img_file,
                        width=canvas_width,
                        height=canvas_height,
                        scale=scale,
                        lineWidth=line_width,
                        )
    
    print('created canvas')
    return canvas


def nav_option(options,label,left_offset,id):
    menu_options = []
    nav_button_input_list = []

    for option in options:
        if option == 'Upload':
            menu = dcc.Upload([dbc.DropdownMenuItem(option)],id=option+'-menu-button')
            
        else:
            menu = dbc.DropdownMenuItem(option,id=option+'-menu-button',n_clicks=0)
        
        menu_options.append(menu)
        nav_button_input_list.append(Input(option+'-menu-button','n_clicks'))
   
    drop_down_menu = html.Div(dbc.DropdownMenu(
        menu_options,
        label=label,
        style={'position':'absolute','margin-left':left_offset},
        id=label+'-menu'
    ),id=label+'-div'
    )

    return drop_down_menu,nav_button_input_list



def brush(classname,id,range,initial_value,title):
     return html.Div([html.H5(title,style={'margin-left':'25%'}),
                    dcc.Slider(range[0],range[1],1,value=initial_value,marks=None,tooltip={"placement":"bottom","always_visible":True},id='Brush-Slider')
                    
                    
                ],id=id,
                    className=classname,style={'border-radius':'20px','margin-top':'78%','position':'absolute','width':'100%','background-color':color_map['blue_clay']}
                    
            
                )



def pallete(classname,color_pallete_id,format,intial_color,title,brush_component,div_id):
  return  html.Div(
                    [
                        html.H5(title,style={'margin-left':'25%'}),
                        brush_component,
                        dmc.ColorPicker(id=color_pallete_id, format=format, value=intial_color,style={'margin-left':'5%','margin-top':'5%','width':'90%','height':'90%'}),
                        dmc.Space(h=10),
                        dmc.Text(id="selected-color"),
                    ]
                    ,id=div_id,className=classname,style={'display':'none'}
                   
                )


def navbar():
    return dbc.NavbarSimple(
                                children=[      
                                ],

                                color="primary",
                                dark=True,
                                style={'height':'5vh','width':'100vw','position':'absolute'}
                            )


def csv_table(classname,column_names,id):
    return html.Div([
                        dash_table.DataTable(id=id,
                                   style_cell={'overflow':'scroll','textAlign':'center','width':'30vw','background-color':color_map['dark_clay']},
                                   columns=[{'name':i ,'id':i} for i in column_names],
                                   style_table={'overflow': 'scroll'}
                                   )
                                    
            
                        ],className=classname,id=id + '-div',
                        style={'border-radius':'10px','margin-top':"77vh",'margin-left':'25vw','width':'51.5vw','background-coolor':'transparent','overflow':'scroll','height':'20vh'}
                    )


def image_canvas(img,brush_size=5,brush_color=None):
    graph_config =  {
                    "modeBarButtonsToAdd": [
                        "drawline",
                        "drawopenpath",
                        "drawclosedpath",
                        "drawcircle",
                        "drawrect",
                        "eraseshape",
                      ]
             }
    figure = canvas_figure(img,brush_size,brush_color)
    return dcc.Graph(id='Canvas',figure=figure,config=graph_config,className='Canvas')

def canvas_figure(img,brush_size=5,brush_color=None):
    figure = px.imshow(img)
    figure.update_layout(dragmode='drawrect')
    figure.update_layout({
        'newshape.line.color':brush_color,
        'newshape.line.width':brush_size,
            })
    
    figure.update_layout(
                xaxis=dict(zeroline=False,showgrid=False,visible=False),
                yaxis=dict(zeroline=False,showgrid=False,visible=False)
                )

    return figure




def create_segmentation_tools(channel_labels=['R','G','B']):
    top_channel_slider = dcc.RangeSlider(0,255,1,value=[0,255],marks=None,tooltip={"placement":"bottom","always_visible":True},id='Top-Channel-Slider',className='Top-Channel-Slider')
    mid_channel_slider = dcc.RangeSlider(0,255,1,value=[0,255],marks=None,tooltip={"placement":"bottom","always_visible":True},id='Mid-Channel-Slider',className='Mid-Channel-Slider')
    bottom_channel_slider = dcc.RangeSlider(0,255,1,value=[0,255],marks=None,tooltip={"placement":"bottom","always_visible":True},id='Bottom-Channel-Slider',className='Bottom-Channel-Slider')

    top_channel_header = html.H1(channel_labels[0],className='Top-Channel-Header')
    mid_channel_header = html.H1(channel_labels[1],className='Mid-Channel-Header')
    bottom_channel_header = html.H1(channel_labels[2],className='Bottom-Channel-Header')

    mask_radio_item = dcc.Checklist(options=['Binary Mask'],value=[],className='Mask-Radio-Item',id='binary-mask')

    segmentation_tool_div = html.Div(children=[top_channel_header,top_channel_slider,mid_channel_header,mid_channel_slider,bottom_channel_header,bottom_channel_slider,mask_radio_item],id='Segmentation-Div',className='Segmentation-Div',style={'display':'none'})
    
    return segmentation_tool_div