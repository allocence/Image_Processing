"This will be a utilty package that handles and file inupt and output that needs to be done"
import os
import shutil
import cv2 as cv
BASE_DIR = 'assets/images'

def upload_file(filename,source_dir='assets/images'):
    """ This function checks to see if the file exists in the correct directory.
        if it does not then it goes ahead and moves the file to the correct directory
        before returning the full path.
        Otherwise it simply returns the full path.

        Inputs:
                filename   (String): The name of the image
                source_dir (String): The source directory where all images should be inorder
                                      for uploading of the image to work.

        Returns:
                full_path  (String): The full path to the source  image
    """
    full_path = os.path.join(source_dir,filename)

    if not os.path.exists(full_path):
         shutil.move(filename,full_path)

       
    return full_path 

def get_image_from_file(filepath,src_dir=BASE_DIR):
     filepath = os.path.join(src_dir,filepath)
     print('filepath: ',filepath)
     image = cv.imread(filepath)
     print("image:",image)
     if image is  None:
          raise FileNotFoundError("Image:" + str(filepath) +" Not Found")
     
     return image