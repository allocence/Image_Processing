def parse_color_space_choice(button_id):
    button_id_parts = button_id.split('-')

    color_space = button_id_parts[0].lower()
    print("button parts: ",button_id_parts)
    print('color space:',color_space)

    return color_space


def parse_canvas_annotation(annotations,class_color_list):
    csv_table_data = []

    try:
        shapes = annotations['shapes']
        for annotation in shapes:
            bounding_box = {}
            bounding_box['x0'] = annotation['x0']
            bounding_box['y0'] = annotation['y0']
            bounding_box['x1'] = annotation['x1']
            bounding_box['y1'] = annotation['y1']

            bounding_box['width'] = width_from_canvas_annotations(bounding_box)
            bounding_box['height'] = height_from_canvas_annotations(bounding_box)
            bounding_box['class'] = class_color_list[annotation['line']['color']]
            csv_table_data.append(bounding_box)
    except:
        return None

    return csv_table_data



def width_from_canvas_annotations(bounding_dictionary):
    return float(bounding_dictionary['x1']) - float(bounding_dictionary['x0'])

def height_from_canvas_annotations(bounding_dictionary):
    return bounding_dictionary['y1'] - bounding_dictionary['y0']