import random
import csv

def random_rgb():
    r = random.randrange(0,255)
    g = random.randrange(0,255)
    b = random.randrange(0,255)

    return 'rgb(' + str(r) + ',' + str(g) + ',' + str(b) + ')'



def random_cv_color():
    random.seed(random.random)

    r = random.randrange(0,255)
    g = random.randrange(0,255)
    b = random.randrange(0,255)

    return (b,g ,r)

def rgba_to_rgb(color:str):
    color_string = str(color)
    rbg,r, = color_string.split('(')
    r,_ = r.split(')')
    test= r.split(',')

    r = test[0]
    g = test[1]
    b = test[0]

    return 'rgb(' + r  + ',' + g +',' +b +')'


def bounding_box_annotation(x1:str,y1:str,width:str,height:str):
    bounds = {}

    bounds['x1'] = x1
    bounds['y1'] = y1
    bounds['x2'] = x1 + width
    bounds['y2'] = y1 + height
    bounds['width'] = width
    bounds['height'] = height

    return bounds


def parse_canvas_json_data(annotation_dictionary:dict):
    csv_dictionary = []
    for annotation in annotation_dictionary:
        data = {}
        data['class'] =  annotation['class']
        data['x1'] = annotation['x1']
        data['y1'] = annotation['y1']
        data['x2'] = annotation['x2']
        data['y2'] = annotation['y2']
        data['width'] = annotation['width']
        data['height'] = annotation['height']


        csv_dictionary.append(data)

    return csv_dictionary